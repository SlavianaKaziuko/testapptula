//
//  AppDelegate.swift
//  TestAppTula
//
//  Created by Slaviana on 11/21/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate {

    var window: UIWindow?
    var tabBarController : UITabBarController?
    
    var allowedOrientantions = UInt(UIInterfaceOrientationMask.portrait.rawValue)
    
    var profile : ProfileModel?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window?.backgroundColor = UIColor.white
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        CoreDataManager.instance.saveContext()
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask(rawValue: allowedOrientantions)
    }
    
    func tabBarControllerSupportedInterfaceOrientations(_ tabBarController: UITabBarController) -> UIInterfaceOrientationMask
    {
        return UIInterfaceOrientationMask(rawValue: allowedOrientantions)
    }

    func setupTabBar()
    {
        self.tabBarController = UITabBarController()
        self.tabBarController!.delegate = self
        let imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        
        let pMain = Storyboards.Main.instantiateMainNavigationController()
        pMain.tabBarItem.image = UIImage(named: "Home")
        pMain.tabBarItem.imageInsets = imageInsets
        
        let pBasket = UINavigationController()
        pBasket.tabBarItem.image = UIImage(named: "Basket")
        pBasket.tabBarItem.imageInsets = imageInsets
        
        let pSearch = UINavigationController()
        pSearch.tabBarItem.image = UIImage(named: "Search")
        pSearch.tabBarItem.imageInsets = imageInsets
        
        let pProfile = Storyboards.Profile.instantiateProfileNavigationController()
        pProfile.tabBarItem.image = UIImage(named: "Profile")
        pProfile.tabBarItem.imageInsets = imageInsets
        
        self.tabBarController!.viewControllers = [pMain, pBasket, pSearch, pProfile]
        UITabBar.appearance().tintColor = Constants.Colors.appMenuSelected
    }
    
    func setupNavBar()
    {
        UINavigationBar.appearance().barTintColor = Constants.Colors.appMainGreen
        UINavigationBar.appearance().backgroundColor = Constants.Colors.appMainGreen
        UINavigationBar.appearance().isOpaque = false
        UINavigationBar.appearance().tintColor = UIColor.white
        
        let titleDict: NSDictionary =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20.0)]
        UINavigationBar.appearance().titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
    }
}

