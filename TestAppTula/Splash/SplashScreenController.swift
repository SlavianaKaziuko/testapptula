//
//  SplashScreenController.swift
//  TestAppTula
//
//  Created by Slaviana on 11/21/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class SplashScreenControler: UIViewController
{
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let appDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.setupTabBar()
        appDelegate.setupNavBar()
        appDelegate.window?.rootViewController = appDelegate.tabBarController
       
    }
}
