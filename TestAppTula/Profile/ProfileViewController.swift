//
//  ProfileViewController.swift
//  TestAppTula
//
//  Created by Slaviana on 11/27/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit
import MobileCoreServices

class ProfileViewController: BaseViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    var profileModel    : ProfileModel?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var buttonChooseAvatar: UIButton!
    
    @IBOutlet weak var textLabeledFirstName: TextFieldLabeled!
    @IBOutlet weak var textLabeledLastName: TextFieldLabeled!
    @IBOutlet weak var textLabeledEmail: TextFieldLabeled!
    
    
    var avatarData: Data?
    var imagePicker: UIImagePickerController = UIImagePickerController()
    var isAvatarChanged = false
    var isAvatarDeleted = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if let profile = appDelegate?.profile
        {
            profileModel = profile
        }
        else
        {
            profileModel = ProfileModel()
            appDelegate?.profile = profileModel
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate?.allowedOrientantions = allowedOrientantions
        
        textLabeledFirstName.addUnderView()
        textLabeledLastName.addUnderView()
        textLabeledEmail.addUnderView()
        
        fillData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.textLabeledFirstName.textField.delegate = self
        self.textLabeledLastName.textField.delegate = self
        self.textLabeledEmail.textField.delegate = self
    }
    
    // MARK: - Actions
    @IBAction func buttonChooseAvatar_Touched(_ sender: UIButton) {
        let alert = UIAlertController(title: "Profile picture", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take a photo", style: .default, handler: { (UIAlertAction) in
            let result = self.startMediaBrowser(from: self, usingDelegate: self, source: .camera)
            print(result.description)
            if !result
            {
                self.showWarningAlert(title: nil, message: "Something went wrong :-(")
            }
        }))
        alert.addAction(UIAlertAction(title: "Use Existing photo", style: .default, handler: { (UIAlertAction) in
            let result = self.startMediaBrowser(from: self, usingDelegate: self, source: .photoLibrary)
            print(result.description)
            if !result
            {
                self.showWarningAlert(title: nil, message: "Something went wrong :-(")
            }
        }))
        
        if profileModel?.avatarImageData != nil
        {
            alert.addAction(UIAlertAction(title: "Remove photo", style: .destructive, handler: { (UIAlertAction) in
                self.imageAvatar.image = UIImage(named: "AvatarDefault")
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        self.present(alert, animated: true)
    }
    
    // MARK: - Text field delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTage=textField.tag + 1;
        // Try to find next responder
        let nextResponder=textField.superview?.superview?.superview?.viewWithTag(nextTage) as UIResponder?
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
            
            // scroll view if keyboard hide active field
            if let activeView = nextResponder as? UIView
            {
                let distanceToBottom = self.scrollView.frame.size.height - getBottomYInScrollView(view: activeView)
                let collapseSpace = (keyboardHeight ?? 0) - distanceToBottom
                if collapseSpace > 0 {
                    // set new offset for scroll view
                    UIView.animate(withDuration: 0.3, animations: {
                        // scroll to the position above keyboard 10 points
                        self.scrollView.contentOffset = CGPoint(x: 0, y: collapseSpace + 10)
                    })
                }
            }
        }
        else
        {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
            // Saving entered data
            saveProfileData()
        }
        
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField
        
        // scroll view if keyboard hide active field
        if let kbHeight = keyboardHeight
        {
            let distanceToBottom = self.scrollView.frame.size.height - getBottomYInScrollView(view: activeField)
            let collapseSpace = (kbHeight) - distanceToBottom
            if collapseSpace > 0 {
                // set new offset for scroll view
                UIView.animate(withDuration: 0.3, animations: {
                    // scroll to the position above keyboard 10 points
                    self.scrollView?.contentOffset = CGPoint(x: 0, y: collapseSpace + 10)
                })
            }
        }
    }
    
    
    // MARK: - Custom adjustments
    func getBottomYInScrollView(view: UIView?) -> CGFloat
    {
        if let viewSelected = view {
            return viewSelected.frame.maxY + viewSelected.convert(CGPoint.zero, to: self.scrollView).y
        }

        return 0
    }

    // MARK: - Edit mode
    @objc func fillData()
    {
        setEditingMode(false)
        
        textLabeledFirstName.text = profileModel?.FirstName
        textLabeledLastName.text = profileModel?.LastName
        textLabeledEmail.text = profileModel?.Email
        
        if let imageData = profileModel?.avatarImageData {
            imageAvatar.image = UIImage(data: imageData)
        }
    }
    
    @objc func setEditingOn()
    {
        setEditingMode(true)
    }
    
    func setEditingMode(_ enabled: Bool = true)
    {
        self.isEditing = enabled
        self.textLabeledFirstName.textField.isEnabled = enabled
        self.textLabeledLastName.textField.isEnabled = enabled
        self.textLabeledEmail.textField.isEnabled = enabled
        
        if !self.isEditing {
            self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(setEditingOn)), animated: false)
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            
            self.navigationItem.setLeftBarButton(nil, animated: false)
            self.buttonChooseAvatar.isEnabled = false
            self.buttonChooseAvatar.isHidden = true
                
            self.textLabeledFirstName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(askIfEditing)))
            self.textLabeledLastName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(askIfEditing)))
            self.textLabeledEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(askIfEditing)))
        }
        else
        {
            self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveProfileData)), animated: false)
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            
            self.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(fillData)), animated: false)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            
            self.buttonChooseAvatar.isEnabled = true
            self.buttonChooseAvatar.isHidden = false
            
            let gestureRecognizerTap = UITapGestureRecognizer(target: self, action: #selector(askIfEditing))
            self.textLabeledFirstName.removeGestureRecognizer(gestureRecognizerTap)
            self.textLabeledLastName.removeGestureRecognizer(gestureRecognizerTap)
            self.textLabeledEmail.removeGestureRecognizer(gestureRecognizerTap)
            
            self.keyboardWillHide(Notification.init(name: UIResponder.keyboardWillHideNotification))
        }
    }
    
    @objc func askIfEditing(_ sender: UIGestureRecognizer)
    {
        if !self.isEditing
        {
            self.keyboardWillHide(Notification.init(name: UIResponder.keyboardWillHideNotification))
            let alert = UIAlertController(title: nil, message: "Do you want to edit the profile?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (UIAlertAction) in
            }))
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
                self.setEditingMode(true)
                if let senderView = sender.view as? TextFieldLabeled
                {
                    senderView.textField.becomeFirstResponder()
                }
            }))
            
            self.present(alert, animated: true, completion: {
            })
        }
    }
    
    // MARK: - Data manipulation
    @objc func saveProfileData()  {
        if textLabeledFirstName.text.isNilOrEmpty
        {
            let alert = UIAlertController(title: nil, message: "Please fill first name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
            }))
            self.present(alert, animated: true, completion: {
                self.textLabeledFirstName.textField.becomeFirstResponder()
            })
            
            return
        }
        
        if textLabeledLastName.text.isNilOrEmpty
        {
            let alert = UIAlertController(title: nil, message: "Please fill last name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                
                self.textLabeledLastName.textField.becomeFirstResponder()
            }))
            self.present(alert, animated: true, completion: {
            })
            
            return
        }
        
        if (textLabeledEmail.text.isNilOrEmpty) || !(profileModel?.isValidEmail(textLabeledEmail.text ?? "") ?? false)
        {
            let alert = UIAlertController(title: nil, message: "Please enter a valid email address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                self.textLabeledEmail.textField.becomeFirstResponder()
            }))
            self.present(alert, animated: true, completion: {
            })
            
            return
        }

        profileModel?.FirstName = textLabeledFirstName.text
        profileModel?.LastName = textLabeledLastName.text
        profileModel?.Email = textLabeledEmail.text
        
        if isAvatarChanged
        {
            profileModel?.avatarImageData = avatarData
        }
        else if isAvatarDeleted
        {
            profileModel?.avatarImageData = nil
        }
        
        profileModel?.saveData()
        setEditingMode(false)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func startMediaBrowser(from controller: UIViewController?, usingDelegate delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)?, source: UIImagePickerController.SourceType) -> Bool {
        if (UIImagePickerController.isSourceTypeAvailable(source) == false) || (delegate == nil) || (controller == nil) {
            return false
        }
        
        let mediaUI = UIImagePickerController()
        mediaUI.sourceType = source
        mediaUI.mediaTypes = UIImagePickerController.availableMediaTypes(for: source)!
        mediaUI.allowsEditing = true
        if source == .camera
        {
            mediaUI.mediaTypes = [kUTTypeImage as String]
            mediaUI.cameraCaptureMode = UIImagePickerController.CameraCaptureMode.photo
        }
        
        mediaUI.delegate = delegate
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad && source == .photoLibrary
        {
            mediaUI.modalPresentationStyle = .popover
            mediaUI.popoverPresentationController?.sourceView = imageAvatar
            mediaUI.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
            mediaUI.popoverPresentationController?.sourceRect = imageAvatar.bounds
            mediaUI.preferredContentSize = CGSize(width: mediaUI.preferredContentSize.width, height: mediaUI.preferredContentSize.width)
            present(mediaUI, animated: true, completion: nil)
        }
        else {
            present(mediaUI, animated: true, completion: nil)
        }
        
        return true
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let mediaType = info[UIImagePickerController.InfoKey.mediaType]
        var originalImage = UIImage()
        var editedImage : UIImage?
        
        if (((mediaType as AnyObject).isEqual("public.image")))
        {
            editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            if editedImage != nil{
                originalImage = editedImage!
            }
            else
            {
                originalImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            }
        }
        
        if originalImage.size.height != originalImage.size.width
        {
            originalImage = originalImage.SquareImage()
        }
        
        let newSize = CGSize(width: imageAvatar.bounds.width, height: imageAvatar.bounds.width)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        originalImage.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        imageAvatar.image = newImage;
        let data = newImage!.pngData()
        avatarData = data?.base64EncodedData(options: .endLineWithLineFeed)
        
        if avatarData != nil
        {
            isAvatarChanged = true
        }
        else
        {
            isAvatarDeleted = true
        }
        
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Keyboard manipulation
    @objc override func keyboardWillShow(_ note: Notification)
    {
        if let userInfo = note.userInfo
        {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            {
                keyboardHeight = keyboardFrame.cgRectValue.size.height
                
                if activeField != nil
                {
                    let distanceToBottom = self.scrollView.frame.size.height - getBottomYInScrollView(view: activeField)
                    let collapseSpace = (keyboardHeight ?? 0) - distanceToBottom
                    if collapseSpace > 0 {
                        // set new offset for scroll view
                        UIView.animate(withDuration: 0.3, animations: {
                            // scroll to the position above keyboard 10 points
                            self.scrollView.setContentOffset(CGPoint(x: 0, y: (collapseSpace + 10)), animated: true)
                        })
                    }
                    
                }
            }
        }
    }
    
    @objc override func keyboardWillHide(_ note: Notification)
    {
        if self.scrollView.contentOffset != CGPoint.zero
        {
            keyboardHeight = 0
            self.scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
}
