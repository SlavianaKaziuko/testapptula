//
//  Storyboards.swift
//  TestAppTula
//
//  Created by Slaviana on 11/21/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

struct Storyboards {
    
    struct Main {
        static let storyboard = UIStoryboard(name: "MainScreens", bundle:nil)
        
        static func instantiateMainNavigationController() -> UINavigationController
        {
            return storyboard.instantiateViewController(withIdentifier: "BooksNavigationController") as! UINavigationController
        }
        
        static func instantiateBooksTableViewController() -> BooksTableViewController
        {
            return storyboard.instantiateViewController(withIdentifier: "BooksTableViewController") as! BooksTableViewController
        }
        
        static func instantiateBooksDetailsViewController() -> BookDetailsViewController
        {
            return storyboard.instantiateViewController(withIdentifier: "BookDetailsController") as! BookDetailsViewController
        }
    }
    
    struct Profile {
        static let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        
        static func instantiateProfileNavigationController() -> UINavigationController
        {
            return storyboard.instantiateViewController(withIdentifier: "ProfileNavigationController") as! UINavigationController
        }
        
        static func instantiateProfileViewController() -> ProfileViewController
        {
            return storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        }
    }
}
