//
//  Books.swift
//  TestAppTula
//
//  Created by Slaviana on 11/22/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class BooksTableViewController: UITableViewController, UITabBarControllerDelegate, BooksModelDelegate
{
    var appDelegate : AppDelegate?
    var allowedOrientantions = UInt(UIInterfaceOrientationMask.allButUpsideDown.rawValue)
    var booksModel : Books?
    
    let identifierHor = "cellHorizontal"
    let identifierVert = "cellVertical"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        appDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        booksModel = Books(delegate: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate?.allowedOrientantions = self.allowedOrientantions
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        
        self.tableView.separatorColor = UIColor.black
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.register(BookCellHorizontal.self, forCellReuseIdentifier: identifierHor)
        self.tableView.register(BookCellVertical.self, forCellReuseIdentifier: identifierVert)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.allowsSelection = false
        
        refreshData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.delegate = self
        appDelegate?.allowedOrientantions = self.allowedOrientantions
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        booksModel?.delegate = self
        reloadData()
    }
    
    func reloadData()
    {
        self.tableView.reloadData()
    }
    
    @objc func refreshData()
    {
        showRefreshControl()
        booksModel?.getByIsbn()
    }
    
    func showRefreshControl()
    {
        if refreshControl != nil && !refreshControl!.isRefreshing
        {
            self.tableView.setContentOffset(CGPoint(x: 0, y: -self.refreshControl!.frame.size.height), animated: true)
            refreshControl?.beginRefreshing()
        }
    }
    
    
    // MARK: - Table
    override func numberOfSections(in tableView: UITableView) -> Int {
        if booksModel?.isDataFetched ?? false
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let booksCount = booksModel?.booksList?.count ?? 0
        print(UIApplication.shared.statusBarOrientation.isPortrait ? (booksCount) : (lround(Double(booksCount) / 2.0)))
        return UIApplication.shared.statusBarOrientation.isPortrait ? (booksCount) : (lround(Double(booksCount) / 2.0))
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.tableView.reloadData()
        super.viewWillTransition(to: size, with: coordinator)
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch UIApplication.shared.statusBarOrientation {
        case .portrait, .portraitUpsideDown:
            let cell = tableView.dequeueReusableCell(withIdentifier: identifierVert, for: indexPath) as! BookCellVertical
            cell.tag = indexPath.row
            cell.innerView.tag = cell.tag
            
            if booksModel?.booksList?.count ?? 0 > indexPath.row
            {
                if let book = booksModel?.booksList?[indexPath.row]
                {
                    cell.fillCellValues(item: indexPath.row, book: book, buttonTap: UITapGestureRecognizer(target: self, action: #selector(cellTouchUpInside)))
                }
            }
            
            return cell
        case .landscapeLeft, .landscapeRight:
            let cell = tableView.dequeueReusableCell(withIdentifier: identifierHor, for: indexPath) as! BookCellHorizontal
            
            for col in 0..<2
            {
                let index = indexPath.row * 2 + col
                if booksModel?.booksList?.count ?? 0 > index
                {
                    if let book = booksModel?.booksList?[index]
                    {
                        cell.innerViews[col].tag = index
                        cell.fillCellValues(item: index, book: book, buttonTap: UITapGestureRecognizer(target: self, action: #selector(cellTouchUpInside)))
                        cell.innerViews[col].innerView.isHidden=false
                    }
                }
                else
                {
                    cell.innerViews[col].innerView.isHidden=true
                }
            }

            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    @objc func cellTouchUpInside(_ sender: UIGestureRecognizer)
    {
        if let index = sender.view?.tag
        {
            let vc = Storyboards.Main.instantiateBooksDetailsViewController()
            if booksModel?.booksList?.count ?? 0 > (index - 1)
            {
                if let book = booksModel?.booksList?[index]
                {
                    vc.bookDetails = book
                    vc.booksModel = booksModel
                    vc.id = index
                    navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    // MARK: - Tab bar controller delegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        if tabBarController.selectedIndex == 1 || tabBarController.selectedIndex == 2 {
            tabBarController.selectedIndex = 0
            let alert = UIAlertController(title: nil, message: "Not implemented yet", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
            }))
            self.present(alert, animated: true, completion: {
                
            })
        }
    }
    
    // MARK: - Api Client delegate
    func getListFinished(error: String?)
    {
        refreshControl?.endRefreshing()
        if error == nil
        {
            print("Books count \(booksModel?.booksList?.count ??  0)")
            self.reloadData()
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alert.show(self, sender: self.tableView)
        }
    }
    
    func getDetailsFinished(error: String?) {
        return
    }
}
