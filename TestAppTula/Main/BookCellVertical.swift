//
//  BookCellVertical.swift
//  TestAppTula
//
//  Created by Slaviana on 12/5/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class BookCellVertical: UITableViewCell {
    
    var innerView = BookItemView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        innerView = BookItemView()
        innerView.initialize(inView: self.contentView)
        self.addSubview(innerView)
    }
    
    func fillCellValues(item: Int, book: BookData, buttonTap: UITapGestureRecognizer)
    {
        innerView.fillValues(book: book, index: item, buttonGesture: buttonTap)
    }
    
}
