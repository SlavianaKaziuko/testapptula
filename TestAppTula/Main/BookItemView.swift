//
//  BooksTableViewCellItem.swift
//  TestAppTula
//
//  Created by Slaviana on 12/4/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class BookItemView: UIView
{
    
    var innerView = UIView()
    
    var imageViewCover: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "BookCoverDefault")
        view.contentMode = .scaleAspectFit
        return view
    }()
    var labelTitle : UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.font = UIFont.systemFont(ofSize: 16.0)
        view.lineBreakMode = .byWordWrapping
        return view
    }()
    var labelAuthor : UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 16.0)
        return view
    }()
    var labelYear : UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 13.0)
        return view
    }()
    var labelPages : UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .right
        view.autoresizingMask = [.flexibleLeftMargin]
        view.font = UIFont.systemFont(ofSize: 13.0)
        return view
    }()
    
    var buttonBookSelect : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func initialize(inView: UIView?)
    {
        self.innerView = inView ?? UIView()
        
        self.innerView.addSubview(imageViewCover)
        self.innerView.addSubview(labelTitle)
        self.innerView.addSubview(labelAuthor)
        self.innerView.addSubview(labelYear)
        self.innerView.addSubview(labelPages)
        self.innerView.addSubview(buttonBookSelect)
        
        addAnchors()
    }
    
    func addAnchors()
    {
        //add anchors for views
        //set default values
        let marginDefault = 8.0 as CGFloat
        let imageWidthDefault = 60.0 as CGFloat
        let marginGuide = innerView.layoutMarginsGuide
        
        imageViewCover.anchor(top: marginGuide.topAnchor, left: marginGuide.leftAnchor, bottom: nil, right: nil,
                              paddingTop: marginDefault, width: imageWidthDefault, height: imageWidthDefault)
        
        labelTitle.anchor(top: imageViewCover.topAnchor, left: imageViewCover.rightAnchor, bottom: nil, right: marginGuide.rightAnchor,
                          paddingTop: 0.5 * marginDefault, paddingLeft: 2 * marginDefault)
        
        labelAuthor.anchor(top: labelTitle.bottomAnchor, left: imageViewCover.rightAnchor, bottom: nil, right: marginGuide.rightAnchor,
                           paddingTop: 0.5 * marginDefault, paddingLeft: 2 * marginDefault)
        
        labelYear.anchor(top: imageViewCover.bottomAnchor, left: marginGuide.leftAnchor, bottom: marginGuide.bottomAnchor, right: nil,
                         paddingTop: marginDefault)
        
        labelPages.anchor(top: labelYear.topAnchor, left: nil, bottom: marginGuide.bottomAnchor, right: marginGuide.rightAnchor)

        buttonBookSelect.anchor(top: innerView.topAnchor, left: innerView.leftAnchor, bottom: innerView.bottomAnchor, right: innerView.rightAnchor)
    }
    
    func fillValues(book: BookData, index: Int, buttonGesture: UIGestureRecognizer)
    {
        self.tag = index
        self.buttonBookSelect.tag = index
        self.buttonBookSelect.addGestureRecognizer(buttonGesture)
        self.setLabelValuesStyled(title: book.title ?? "", author: book.authors?.first?.name ?? "")
        
        self.labelYear.text = book.publishDate
        self.labelYear.sizeToFit()
        
        self.labelPages.text = "pages: \(book.numberOfPages ?? 0)"
        self.labelPages.sizeToFit()
        
        let coverUrl = book.getImageUrl(coverSize: .smallest) ?? ""
        if !coverUrl.isNilOrEmpty {
            ImageCache.getImage(tag: self.tag, urlStr: coverUrl) { (tag, downloadedImage, error) in
                DispatchQueue.main.async() {
                    if !(error?.isEmpty ?? true)
                    {
                        #if DEBUG
                        print(error ?? "")
                        #endif
                    }
                    
                    if self.tag == tag
                    {
                        if let imResieved = downloadedImage
                        {
                            self.imageViewCover.image = imResieved
                        }
                        else
                        {
                            self.imageViewCover.image = UIImage(named: "BookCoverDefault")
                        }
                    }
                    else
                    {
                        print("Wrong cell")
                    }
                }
            }
        }
        else
        {
            print("empty url")
            self.imageViewCover.image = UIImage(named: "BookCoverDefault")
        }
    }
    
    func setLabelValuesStyled(title: String?, author: String?)
    {
        let bulletPoint: String = "\u{2022}"
        let attributtedBullet = NSAttributedString(string: "\(bulletPoint)\t", attributes: [NSAttributedString.Key.foregroundColor : Constants.Colors.appMainGreen, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16.0)])
        let paragraphStyle: NSMutableParagraphStyle
        paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 20, options: [NSTextTab.OptionKey.columnTerminators : ""])]
        paragraphStyle.defaultTabInterval = 20
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.headIndent = 20
        
        let attributedStringTitle = NSMutableAttributedString(attributedString: attributtedBullet)
        attributedStringTitle.append(NSAttributedString(string: title ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)]))
        attributedStringTitle.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedStringTitle.length))
        self.labelTitle.attributedText = attributedStringTitle
        self.labelTitle.sizeToFit()
        
        let attributedStringAuthor = NSMutableAttributedString(attributedString: attributtedBullet)
        attributedStringAuthor.append(NSAttributedString(string: author ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)]))
        attributedStringAuthor.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedStringAuthor.length))
        self.labelAuthor.attributedText = attributedStringAuthor
        self.labelAuthor.sizeToFit()
        
        
    }
}
