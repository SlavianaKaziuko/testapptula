//
//  BookCellHorizontal.swift
//  TestAppTula
//
//  Created by Slaviana on 12/5/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class BookCellHorizontal: UITableViewCell {
    
    var innerViews = [BookItemView]()
    
    let divider: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        drawLayout()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        drawLayout()
    }
    
    func drawLayout()
    {
        let cellLeftSubview = UIView()
        cellLeftSubview.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(cellLeftSubview)
        let cellRightSubveiw = UIView()
        self.contentView.addSubview(cellRightSubveiw)
                cellLeftSubview.anchor(top: contentView.topAnchor,
                               left: contentView.leftAnchor,
                               bottom: contentView.bottomAnchor,
                               right: cellRightSubveiw.leftAnchor)
        cellRightSubveiw.anchor(top: contentView.topAnchor,
                                left: cellLeftSubview.rightAnchor,
                                bottom: contentView.bottomAnchor,
                                right: contentView.rightAnchor)
        cellRightSubveiw.widthAnchor.constraint(equalTo:cellLeftSubview.widthAnchor).isActive = true
        
        let cellLeft = BookItemView()
        cellLeft.translatesAutoresizingMaskIntoConstraints = false
        cellLeft.initialize(inView: cellLeftSubview)
        cellLeft.isHidden = true
        innerViews.append(cellLeft)
        
        let cellRight = BookItemView()
        cellRight.translatesAutoresizingMaskIntoConstraints = false
        cellRight.initialize(inView: cellRightSubveiw)
        cellRight.isHidden = true
        innerViews.append(cellRight)
        
        cellLeftSubview.addDividerRight(color: UIColor.black, width: 0.5)
    }
    
    func fillCellValues(item: Int, book: BookData, buttonTap: UITapGestureRecognizer)
    {
        if innerViews.count > (item % 2)
        {
            let cell = innerViews[item % 2]
            cell.isHidden = false
            cell.fillValues(book: book, index: item, buttonGesture: buttonTap)
        }
    }
}
