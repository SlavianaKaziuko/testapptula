//
//  BookDetailsViewController.swift
//  TestAppTula
//
//  Created by Slaviana on 11/23/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class BookDetailsViewController : BaseViewController, BooksModelDelegate
{
    var booksModel : Books?
    var bookDetails : BookData?
    var id: Int?

    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var labelYear: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelDescriptionLabel: UILabel!
    @IBOutlet weak var buttonOpenInfo: UIButton!
    @IBOutlet weak var activityIndDetatailsLoading: UIActivityIndicatorView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        allowedOrientantions = UInt(UIInterfaceOrientationMask.all.rawValue)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        booksModel?.delegate = self
    }
    
    // MARK: - Actions
    @IBAction func buttonOpenInfo_Touched(_ sender: Any)
    {
        if let url = URL(string: bookDetails?.url ?? "")
        {
            if UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.open(url, completionHandler: { (success) in
                    print("Details opened: \(success)") // Prints true
                    if success != true
                    {
                        
                    }
                })
            }
        }
    }
    
    // MARK: - Books Model delegate
    func getListFinished(error: String?) {
        return
    }
    
    func getDetailsFinished(error: String?) {
        if let identifier = id
        {
            activityIndDetatailsLoading.stopAnimating()
            activityIndDetatailsLoading.isHidden = true
            bookDetails = booksModel?.booksList?[identifier]
            if let descr = bookDetails?.description
            {
                labelDescription.text = descr
                labelDescription.sizeToFit()
                labelDescriptionLabel.isHidden = false
            }
            else
            {
                labelDescription.text = bookDetails?.subtitle ?? "No description available"
                labelDescription.textColor = UIColor.gray
                labelDescriptionLabel.isHidden = true
            }
        }
    }
    
    // MARK: - custom view updates
    func fillData()
    {
        if let book = bookDetails
        {
            activityIndDetatailsLoading.hidesWhenStopped = true
            if book.description.isNilOrEmpty
            {
                booksModel?.getDetails(by: "\(book.getIdentifier() ?? "")")
                activityIndDetatailsLoading.isHidden = false
                activityIndDetatailsLoading.startAnimating()
            }
            else
            {
                activityIndDetatailsLoading.isHidden = true
            }
            
            labelName.text = book.title ?? "Book name"
            labelAuthor.text = book.getAuthors() ?? ""
            labelYear.text = book.publishDate ?? ""
            
            if let descr = book.description
            {
                labelDescription.text = descr
                labelDescription.sizeToFit()
                labelDescriptionLabel.isHidden = false
            }
            else
            {
                labelDescriptionLabel.isHidden = true
            }
            
            labelDescription.font = UIFont.italicSystemFont(ofSize: 15.0)
            labelPrice.isHidden = true
            
            self.title = book.title ?? "BOOK DETAILS"
            
            let coverImageUrl = book.getImageUrl(coverSize: .largest) ?? ""
            if !coverImageUrl.isNilOrEmpty
            {
                ImageCache.getImage(tag: id ?? 0, urlStr: coverImageUrl) { (tag, downloadedImage, error) in
                    DispatchQueue.main.async() {
                        if !(error?.isEmpty ?? true)
                        {
                            #if DEBUG
                            print(error ?? "")
                            #endif
                        }
                        
                        self.imageCover.image = downloadedImage ?? UIImage(named: "BookCoverDefault")
                    }
                }
            }
            else
            {
                self.imageCover.image = UIImage(named: "BookCoverDefault")
            }
            
            buttonOpenInfo.isEnabled = (URL(string: book.url ?? "") != nil)
        }
    }
}
