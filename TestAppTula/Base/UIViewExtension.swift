//
//  UIViewExtension.swift
//  TestAppTula
//
//  Created by Slaviana on 11/25/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit
extension UIView {
    
    func anchor (top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,
                 paddingTop: CGFloat = 0, paddingLeft: CGFloat = 0, paddingBottom: CGFloat = 0, paddingRight: CGFloat = 0,
                 width: CGFloat = 0, height: CGFloat=0) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
    }
    
    func addBorder(color: CGColor, width: CGFloat)
    {
        self.layer.cornerRadius = 0.0
        self.layer.borderColor = color
        self.layer.borderWidth = width
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        
    }
    
    func addDividerLeft(color: UIColor, width: CGFloat)
    {
        let view = UIView()
        view.backgroundColor = color
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        view.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        view.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func addDividerRight(color: UIColor, width: CGFloat)
    {
        let view = UIView()
        view.backgroundColor = color
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        view.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        view.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
}
