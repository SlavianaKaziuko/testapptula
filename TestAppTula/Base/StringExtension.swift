//
//  StringExtension.swift
//  TestAppTula
//
//  Created by Slaviana on 12/1/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import Foundation

extension String
{
    var isNilOrEmpty : Bool
    {
        get {
            return self.isEmpty
        }
    }
}


protocol OptionalString {}
extension String: OptionalString {}

extension Optional where Wrapped: OptionalString
{
    var isNilOrEmpty: Bool
    {
        return ((self as? String) ?? "").isEmpty
    }
}
