//
//  TextFieldLabeled.swift
//  TestAppTula
//
//  Created by Slaviana on 11/27/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class TextFieldLabeled: UIView {
    
    @IBOutlet weak var textField : UITextField!
    @IBOutlet weak var label     : UILabel!
    var underView : UIView?
    
    var text : String?
    {
        get
        {
            return self.textField.text
        }
        set(value)
        {
            self.textField.text = value
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func addUnderView()
    {
        if underView == nil
        {
            underView = UIView(frame: CGRect.zero)
            self.addSubview(underView!)
            
            underView?.backgroundColor = UIColor.black
            underView?.anchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
            underView?.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            underView?.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
            
        }
    }
}
