//
//  BaseViewController.swift
//  TestAppTula
//
//  Created by Slaviana on 12/6/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UITabBarControllerDelegate {
    
    var allowedOrientantions: UInt
    
    var appDelegate     : AppDelegate?
    
    var keyboardHeight : CGFloat?
    var activeField : UITextField?
    
    required init?(coder aDecoder: NSCoder) {
        allowedOrientantions = UInt(UIInterfaceOrientationMask.portrait.rawValue)
        super.init(coder: aDecoder)
        appDelegate = (UIApplication.shared.delegate) as? AppDelegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.Colors.lightBackground
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDelegate?.allowedOrientantions = self.allowedOrientantions
        if UIInterfaceOrientationMask(rawValue: allowedOrientantions) == .portrait
        {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        }
        
        self.tabBarController?.delegate = self
        registerKeyboardObserver()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        unregisterKeyboardObserver()
    }
    
    // MARK: - keyBoardManipulations
    func registerKeyboardObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_ :)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_ :)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func unregisterKeyboardObserver()
    {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ note: Notification)
    {
        
    }
    
    @objc func keyboardWillHide(_ note: Notification)
    {
        
    }
    
    // MARK: - Tab bar controller
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        if tabBarController.selectedIndex == 1 || tabBarController.selectedIndex == 2 {
            tabBarController.selectedIndex = 3
            self.showNotImplementedAlert()
        }
    }
    
    func showNotImplementedAlert()
    {
        self.showWarningAlert(title: nil, message: "Not implemented yet")
    }
    
    
    func showWarningAlert(title: String?, message: String?)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (UIAlertAction) in
        }))
        self.present(alert, animated: true, completion: {
        })
    }
}
