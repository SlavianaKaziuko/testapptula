//
//  ProfileModel.swift
//  TestAppTula
//
//  Created by Slaviana on 11/28/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit
import CoreData

class ProfileModel {
    let managedProfile : ProfileData
    let fetchedResultsController : NSFetchedResultsController<NSFetchRequestResult>
    
    var FirstName: String?
    var LastName: String?
    var Email: String?
    var avatarImageData: Data?
    
    init()
    {
        managedProfile = ProfileData()
        fetchedResultsController = CoreDataManager.instance.fetchedResultsController(fetchRequest: ProfileData.fetchRequestTopRecords(top: 1))
        
        getProfileRecords(top: 1)
    }
    
    func getProfileRecords(top: Int)
    {
        do
        {
            try fetchedResultsController.performFetch()
            let result = fetchedResultsController.fetchedObjects?.first as! ProfileData
            FirstName = result.firstName
            LastName = result.lastName
            Email = result.email
            if let avatarNSData = result.avatar
            {
                avatarImageData = Data(referencing: avatarNSData)
            }
        }
        catch let error
        {
            print(error.localizedDescription)
        }
    }
    
    func isValidEmail(_ candidate: String) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let test = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return test.evaluate(with: candidate)
    }
    
    func saveData()
    {
        managedProfile.firstName = FirstName
        managedProfile.lastName = LastName
        managedProfile.email = Email
        managedProfile.updateDate = Date.init() as NSDate
        if let avatarData = avatarImageData
        {
            managedProfile.avatar = NSData(base64Encoded: avatarData, options: .ignoreUnknownCharacters)
        }
        else
        {
            managedProfile.avatar = nil
        }
        
        CoreDataManager.instance.saveContext()
    }
}
