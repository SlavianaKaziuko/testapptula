//
//  Constants.swift
//  TestAppTula
//
//  Created by Slaviana on 11/25/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

struct Constants {
    struct Colors {
        static let appMainGreen : UIColor = UIColor.init(red: 0.384, green: 0.612, blue: 0.267, alpha: 1.0)
        static let appMenuSelected : UIColor = UIColor.init(red: 148.0 / 255.0, green: 210.0 / 255.0, blue: 115.0 / 255.0, alpha: 1.0)
        static let lightBackground : UIColor = UIColor.init(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
        
    }
}
