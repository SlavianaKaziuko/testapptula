//
//  BooksModel.swift
//  TestAppTula
//
//  Created by Slaviana on 11/24/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import Foundation

struct Author : Codable {
    var url : String?
    var name : String?
}

struct Identifiers : Codable {
    var openlibrary: [String]?
    var isbn10: [String]?
    var isbn13: [String]?
    var alibrisId: [String]?
    
    enum CodingKeys: String, CodingKey {
        case openlibrary
        case isbn10 = "isbn_10"
        case isbn13 = "isbn_13"
        case alibrisId = "alibris_id"
    }
}

struct Cover : Codable {
    var small : String?
    var medium : String?
    var large : String?
    
    enum CoverSize : String {
        case small
        case medium
        case large
        case smallest
        case largest
    }
}

struct BookData : Codable {
    var title : String?
    var subtitle : String?
    var ISBN : String?
    var authors : [Author]?
    var cover : Cover?
    var url: String?
    var identifiers: Identifiers?
    var publishDate : String?
    var numberOfPages : Int?
    var description : String?
    
    enum CodingKeys : String, CodingKey {
        case title
        case subtitle
        case ISBN
        case authors
        case cover
        case url
        case identifiers
        case publishDate = "publish_date"
        case numberOfPages = "number_of_pages"
        case description
    }
    
    func getImageUrl(coverSize: Cover.CoverSize?) -> String?
    {
        var urlStr = ""
        switch coverSize ?? .smallest {
        case .small:
            urlStr = cover?.small ?? ""
            break
        case .medium:
            urlStr = cover?.medium ?? ""
            break
        case .large:
            urlStr = cover?.large ?? ""
            break
            
        case .smallest:
            urlStr = ((cover?.small ?? cover?.medium) ?? cover?.large) ?? ""
            break
            
        case .largest:
            urlStr = ((cover?.large ?? cover?.medium) ?? cover?.small) ?? ""
            break
        }
        
        return urlStr
    }
    
    func getIdentifier() -> String?
    {
        var id = ""
        
        if self.identifiers?.isbn13?.count ?? 0 > 0 || self.identifiers?.isbn10?.count ?? 0 > 0 {
            id = "ISBN:"
            if let isbn13 = self.identifiers?.isbn13?.first
            {
                id = id + isbn13
            }
            else if let isbn10 = self.identifiers?.isbn10?.first
            {
                id = id + isbn10
            }
        }
        else if self.identifiers?.openlibrary?.count ?? 0 > 0
        {
            id = "OLID:\(self.identifiers?.openlibrary?.first ?? "")"
        }
        
        return id
    }
    
    func getDescription() -> String?
    {
        return (self.description ?? subtitle)
    }
    
    func getAuthors() -> String?
    {
        return authors?.compactMap({ $0.name }).joined(separator: ", ")
    }
    
    mutating func updateDetails(bookWithDetails: BookDetails)
    {
        self.description = bookWithDetails.description
    }
}

struct DescriptionDetails : Codable {
    var type: String?
    var value: String?
}

struct BookDetails : Decodable {
    var description: String?
    var isbn10: [String]?
    var isbn13: [String]?
    var identifiers: Identifiers?
    
    enum CodingKeys : String, CodingKey {
        case description
        case isbn10 = "isbn_10"
        case isbn13 = "isbn_13"
        case identifiers
    }
    
    init(from decoder: Decoder) throws {
        do{
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            if let isbnArray = try? container.decode([String].self, forKey: .isbn10) {
                isbn10 = isbnArray
            }
            if let isbnArray = try? container.decode([String].self, forKey: .isbn13) {
                isbn13 = isbnArray
            }
            if let identifiersParsed = try? container.decode(Identifiers.self, forKey: .identifiers) {
                identifiers = identifiersParsed
            }
            
            if let stringProperty = try? container.decode(String.self, forKey: .isbn10) {
                description = stringProperty
            } else if let propertyFromDict = try? container.decode(DescriptionDetails.self, forKey: .description) {
                description = propertyFromDict.value
            } else {
                throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: container.codingPath, debugDescription: "Not a JSON"))
            }
        }
    }
    
    func getIdentifier() -> String?
    {
        var id = ""
        
        if self.identifiers?.isbn13?.count ?? 0 > 0 || self.identifiers?.isbn10?.count ?? 0 > 0 {
            id = "ISBN:"
            if let isbn13 = self.identifiers?.isbn13?.first
            {
                id = id + isbn13
            }
            else if let isbn10 = self.identifiers?.isbn10?.first
            {
                id = id + isbn10
            }
        }
        else if self.identifiers?.openlibrary?.count ?? 0 > 0
        {
            id = "OLID:\(self.identifiers?.openlibrary?.first ?? "")"
        }
        else if self.isbn10?.count ?? 0 > 0 || self.isbn13?.count ?? 0 > 0
        {
            id = "ISBN:"
            if let isbn13 = self.isbn13?.first
            {
                id = id + isbn13
            }
            else if let isbn10 = self.isbn10?.first
            {
                id = id + isbn10
            }
        }
        
        return id
    }
}
    
struct BookDetailsResponse : Decodable {
    var bibKey: String?
    var details: BookDetails?
    var infoUrl: String?
    
    enum CodingKeys : String, CodingKey {
        case bibKey = "bib_key"
        case details
        case infoUrl = "info_url"
    }

    init(from decoder: Decoder) throws {
        do{
            let container = try decoder.container(keyedBy: CodingKeys.self)
            if let key = try? container.decode(String.self, forKey: .bibKey) {
                bibKey = key
            }
            
            if let detailsParsed = try? container.decode(BookDetails.self, forKey: .details) {
                details = detailsParsed
            }
            
            if let infoUrlParsed = try? container.decode(String.self, forKey: .infoUrl) {
                infoUrl = infoUrlParsed
            }
        }
    }
}

protocol BooksModelDelegate {
    func getListFinished(error: String?) -> Void
    func getDetailsFinished(error: String?) -> Void
    
}

class Books: ApiClientDelegate {
    var delegate : BooksModelDelegate?
    var booksList : [BookData]?
    
    var isDataFetched : Bool = false
    
    init()
    {
        booksList = [BookData]()
    }
    
    init(delegate: BooksModelDelegate)
    {
        booksList = Array<BookData>()
        self.delegate = delegate
    }
    
    func getByIsbn()
    {
        let isbns=["9780385472579",
                   "9780590353403",
                   "9780439203531",
                   "9785353003090",
                   "9780439136358",
                   "9785353005797",
                   "9780545010221",
                   "9780060987015",
                   "9781559390736",
                   "0861711505",
                   "1573228834",
                   "9781573221115",
                   "9785446107766"]
        
        let apiClient = ApiClient(delegate: self, selector: ApiSelector.getBooksData)
        apiClient.getBooksList(by: isbns)
        isDataFetched = false
    }
    
    func getDetails(by identifier: String)
    {
        let apiClient = ApiClient(delegate: self, selector: ApiSelector.getBookDetails)
        apiClient.getBookDetails(by: identifier)
    }
    
    func apiRequestFinished(error: String?, selector: ApiSelector, responseData: Data?)
    {
        if error != nil
        {
            return
        }
        
        switch selector {
        case .getBooksData:
            if let data = responseData
            {
                do{
                    booksList?.removeAll()
                    
                    let decoder = JSONDecoder()
                    let jsonResponse = try decoder.decode(Dictionary<String, BookData>.self, from: data)
                    
                    for book in jsonResponse
                    {
                        booksList!.append(book.value)
                    }
                    
                    isDataFetched = true
                    delegate?.getListFinished(error: error)
                }
                catch let fatalError as NSError
                {
                    #if DEBUG
                        print(fatalError)
                    #endif
                }
            }
            break
            
        case .getBookDetails:
            if let data = responseData
            {
                do{
                    let decoder = JSONDecoder()
                    let jsonResponse = try decoder.decode(Dictionary<String, BookDetailsResponse>.self, from: data)
                    
                    for bookDetails in jsonResponse
                    {
                        for i in 0..<(booksList?.count ?? 0)
                        {
                            if booksList?[i].getIdentifier() == bookDetails.value.details?.getIdentifier()
                            {
                                if let newData = bookDetails.value.details
                                {
                                    booksList?[i].updateDetails(bookWithDetails: newData)
                                }
                            }
                        }
                    }
                    
                    isDataFetched = true
                    delegate?.getDetailsFinished(error: error)
                }
                catch let fatalError as NSError
                {
                    #if DEBUG
                    print(fatalError)
                    #endif
                }
            }
            break
        }
    }
}
