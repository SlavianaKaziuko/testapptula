//
//  ProfileData+CoreDataProperties.swift
//  TestAppTula
//
//  Created by Slaviana on 11/30/18.
//  Copyright © 2018 TestApp. All rights reserved.
//
//

import Foundation
import CoreData

extension ProfileData {
    
    @NSManaged public var email: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var updateDate: NSDate?
    @NSManaged public var avatar: NSData?

    @nonobjc public class func fetchRequestTopRecords(top: Int) -> NSFetchRequest<NSFetchRequestResult>
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProfileData")
        request.sortDescriptors = [NSSortDescriptor(key: "updateDate", ascending: false)]
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = top
        
        return request
    }
    
    @nonobjc public class func fetchRequestAll() -> NSFetchRequest<NSFetchRequestResult> {
        return NSFetchRequest<NSFetchRequestResult>(entityName: "ProfileData")
    }
}
