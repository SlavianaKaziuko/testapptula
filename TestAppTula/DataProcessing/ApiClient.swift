//
//  ApiClient.swift
//  TestAppTula
//
//  Created by Slaviana on 11/24/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

enum ApiSelector {
    case getBooksData
    case getBookDetails
}

protocol ApiClientDelegate {
    func apiRequestFinished(error: String?, selector: ApiSelector, responseData: Data?)
}

class ApiClient : ApiClientDelegate
{
    var delegate    : ApiClientDelegate
    let baseUrl     : String = "https://openlibrary.org/"
    
    var urlSeccion          : URLSession?
    var errorDescription    : String?
    var jsonResponse        : Dictionary<String, AnyObject?>?
    var dataResponse        : Data?
    var selector            : ApiSelector

    init(delegate: ApiClientDelegate, selector: ApiSelector) {
        self.delegate = delegate
        self.selector = selector
    }
    
    func requestApiCall(_ httpMethod: String, query: String) -> String?
    {
        let config = URLSessionConfiguration.default
        let sessionData = URLSession(configuration: config)
        
        let urlString = baseUrl + query
        guard let url = URL(string: urlString)
        else {
            return "Bad URL: \(urlString)"
        }
        
        #if DEBUG
            print(urlString)
        #endif
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let dataTask : URLSessionDataTask = sessionData.dataTask(with: url) {
            (data, response, error) in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
                if (error != nil)
                {
                    self.errorDescription = error?.localizedDescription ?? ""
                }
                else
                {
                    if(data == nil) || (data?.count ?? 0 == 0)
                    {
                        self.errorDescription = "Response has no data"
                    }
                    else
                    {
                        self.dataResponse = data
                    }
            }
            
            DispatchQueue.main.async {
                self.apiRequestFinished(error: self.errorDescription, selector: self.selector, responseData: self.dataResponse)
            }
            
        }
        
        dataTask.resume()
        
        return errorDescription
    }
    
    func getBooksList (by isbnList: [String])
    {
        let query = "api/books?bibkeys=ISBN:\(isbnList.joined(separator: ","))&format=json&jscmd=data"
        
        if let res = requestApiCall("GET", query: query) {
            #if DEBUG
                print(res)
            #endif
        }
    }
    
    func getBookDetails(by identifier: String)
    {
        let query = "api/books?bibkeys=\(identifier)&format=json&jscmd=details"
        
        if let res = requestApiCall("GET", query: query) {
            #if DEBUG
            print(res)
            #endif
        }
    }
    
    func apiRequestFinished(error: String?, selector: ApiSelector, responseData: Data?) {
        if error != nil && !(error?.isEmpty ?? true)
        {
            #if DEBUG
                print(error ?? "")
            #endif
            return
        }
        
        delegate.apiRequestFinished(error: error, selector: selector, responseData: responseData)
    }
}
