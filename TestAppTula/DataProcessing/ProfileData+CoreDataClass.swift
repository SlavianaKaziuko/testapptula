//
//  ProfileData+CoreDataClass.swift
//  TestAppTula
//
//  Created by Slaviana on 11/30/18.
//  Copyright © 2018 TestApp. All rights reserved.
//
//

import Foundation
import CoreData

@objc(ProfileData)
public class ProfileData: NSManagedObject {
    
    private convenience init()
    {
        let entity = NSEntityDescription.entity(forEntityName: "ProfileData", in: CoreDataManager.instance.managedObjectContext)
        
        self.init(entity: entity!, insertInto: CoreDataManager.instance.managedObjectContext)
    }
}
