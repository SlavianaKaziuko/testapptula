//
//  ImageCache.swift
//  TestAppTula
//
//  Created by Slaviana on 11/27/18.
//  Copyright © 2018 TestApp. All rights reserved.
//

import UIKit

class ImageCache {
    
    static let coversCache = NSCache<NSString, UIImage>()
    
    static func getImage(tag: Int, urlStr: String, completion: @escaping(_ tag: Int, _ data: UIImage?, _ error: String?) -> Void)
    {
        
        if let readyImage = coversCache.object(forKey: NSString(string: urlStr))
        {
            completion(tag, readyImage, nil)
        }
        else
        {
            #if DEBUG
                print("download: \(urlStr)")
            #endif
            
            guard let url = URL(string: urlStr)
                else
            {
                completion(tag, nil, "Bad URL")
                return
            }
            
            URLSession.shared.dataTask(with: url, completionHandler: { (data, responseUrl, error) -> Void in
                if let error = error {
                    completion(tag, nil, error.localizedDescription)
                    
                } else if let data = data, let image = UIImage(data: data) {
                    ImageCache.coversCache.setObject(image, forKey: NSString(string: urlStr))
                    completion(tag, image, nil)
                } else {
                    completion(tag, nil, "Cover image data parsing error!")
                }
            }).resume()
        }
    }
}
